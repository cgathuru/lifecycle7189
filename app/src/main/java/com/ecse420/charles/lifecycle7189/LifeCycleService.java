package com.ecse420.charles.lifecycle7189;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.widget.Toast;

public class LifeCycleService extends Service {
    int mStartMode;       // indicates how to behave if the service is killed
    private final IBinder mBinder =  new LocalBinder();      // interface for clients that bind
    boolean mAllowRebind; // indicates whether onRebind should be used


    public class LocalBinder extends Binder {
        LifeCycleService getService() {
            return LifeCycleService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Toast.makeText(getApplicationContext(),"Service onCreate called", Toast.LENGTH_SHORT).show();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Toast.makeText(getApplicationContext(),"Service onStartCommand called", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // A client is binding to the service with bindService()
        Toast.makeText(getApplicationContext(),"Service onBind called", Toast.LENGTH_SHORT).show();
        return mBinder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Toast.makeText(getApplicationContext(),"Service onUnbind called", Toast.LENGTH_SHORT).show();
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        Toast.makeText(getApplicationContext(),"Service onRebind called", Toast.LENGTH_SHORT).show();
        super.onRebind(intent);
    }

    @Override
    public void onDestroy() {
        Toast.makeText(getApplicationContext(),"Service onDestroy called", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
}